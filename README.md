O **MORASIFE** é um Modelo de Referência para Avaliação de Simuladores de Física para Fins Educacionais formado a partir de requisitos pedagógicos aplicados à Norma ISO/IEC 25010:2011.

Autores:
Roges Horacio Grandi
Manuel Joaquim Silva Oliveira
José Valdeni de Lima
Leandro Krug Wives
Raquel Salcedo Gomes
